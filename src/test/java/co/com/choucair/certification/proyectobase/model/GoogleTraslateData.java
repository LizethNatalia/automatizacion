package co.com.choucair.certification.proyectobase.model;

public class GoogleTraslateData {
  private String origin;
  private String destination;
  private String wordi;
  private String worde;

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getWordi() {
        return wordi;
    }

    public void setWordi(String wordi) {
        this.wordi = wordi;
    }

    public String getWorde() {
        return worde;
    }

    public void setWorde(String worde) {
        this.worde = worde;
    }
}
