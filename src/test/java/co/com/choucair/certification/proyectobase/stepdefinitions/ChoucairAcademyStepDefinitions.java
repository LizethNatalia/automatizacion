package co.com.choucair.certification.proyectobase.stepdefinitions;

import co.com.choucair.certification.proyectobase.model.GoogleTraslateData;
import co.com.choucair.certification.proyectobase.questions.Answer;
import co.com.choucair.certification.proyectobase.tasks.Login;
import co.com.choucair.certification.proyectobase.tasks.OpenUp;
import co.com.choucair.certification.proyectobase.tasks.Search;
import co.com.choucair.certification.proyectobase.tasks.Translate1;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.GivenWhenThen;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;

public class ChoucairAcademyStepDefinitions {
    private Actor yeison = Actor.named("Yeison");


    @Before
    public void setStage(){
        OnStage.setTheStage(new OnlineCast());
    }

    @Given("^than Lizeth wants to learn automation at the academy Choucair$")
    public void thanLizethWantsToLearnAutomationAtTheAcademyChoucair()  {
        OnStage.theActorCalled("Lizeth").wasAbleTo(OpenUp.thePage(), (Login.onThePage()));

    }


    @When("^he search for the course (.*) on the Choucair Academy platform$")
    public void heSearchForTheCourseMetodologíaBancolombiaOnTheChoucairAcademyPlatform(String course) {
      OnStage.theActorInTheSpotlight().attemptsTo(Search.the(course));
    }

    @When("^he translates a word (.*) from English to Spanish$")
    public void heTranslatesAWordFromEnglishToSpanish(List<GoogleTraslateData> GoogletraslateData ){
        yeison.attemptsTo(Translate1.TranslateFromEnglishToSpanish());
    }


    @Then("^he finds the course called resources (.*)$")
    public void heFindsTheCourseCalledResourcesMetodologíaBancolombia(String question) {
        OnStage.theActorInTheSpotlight().should(GivenWhenThen.seeThat(Answer.toThe(question)));
    }

}
